package net.wws.data_util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.wws.tally.R;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.TextView;

/*
 * SharedPreferences的getStringSet方法得到的Set集合中的元素的顺序并不保证与调用Editor.putStringSet方法时的一致，
 即使用的是LinkedHashSet（血的教训，调试了很久。变态的特性!!!）
 */

public class DAOUtil {

	public static TallyDatabaseHelper dbHelper = null;

	public static boolean addTally(Context context, String tally_name_str,
			String tally_name_remark_str) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 先检查数据库中是否已经有了此账本
		String querySQL = "select tally_name from tally_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new
													// String[]{}
		while (cursor.moveToNext()) {
			String tmpTallyName = cursor.getString(0);
			if (tmpTallyName.equals(tally_name_str))
				return false;
		}

		// 第一列设置了autoincrement属性，所以插入记录是为之赋null
		String insertSQL = "insert into tally_table values(null,?,?,?)";
		String dateTime = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss",
				Locale.CHINA).format(new Date());
		db.execSQL(insertSQL, new String[] { tally_name_str,
				tally_name_remark_str, dateTime });
		return true;
	}

	public static List<List<String>> getAllTally(Context context) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String querySQL = "select tally_name,remark from tally_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new

		List<List<String>> tallys = new ArrayList<List<String>>();
		// 取出账本的相关信息作为列表项的一部分
		while (cursor.moveToNext()) {
			List<String> tally = new ArrayList<String>();
			tally.add(cursor.getString(0));
			tally.add(cursor.getString(1));
			tallys.add(tally);
		}
		return tallys;
	}

	public static boolean deleteTally(View itemView) {

		String tallyName = ((TextView) (itemView.findViewById(R.id.tally_name)))
				.getText().toString();
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(itemView.getContext(), "tallys",
					null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 根据账本名称获取id
		String queryTallyId = "select id from tally_table where tally_name=?";
		Cursor tally_cursor = db.rawQuery(queryTallyId,
				new String[] { tallyName });
		tally_cursor.moveToNext();
		String tally_id = tally_cursor.getString(0);

		// 删除该账本下的所有记录
		String deleteRecord = "delete from record_table where id=?";
		db.execSQL(deleteRecord, new String[] { tally_id });

		// 删除账本
		String deleteSQL = "delete from tally_table where tally_name = ?";
		db.execSQL(deleteSQL, new String[] { tallyName });

		return true;
	}

	public static boolean editTally(View itemView, String tallyNewName,
			String tallyNewRemark) {
		Context context = itemView.getContext();
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String tallyOldName = ((TextView) itemView
				.findViewById(R.id.tally_name)).getText().toString();

		// 先检查数据库中是否已经有了此账本
		String querySQL = "select tally_name from tally_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new
													// String[]{}
		while (cursor.moveToNext()) {
			String tmpTallyName = cursor.getString(0);
			if (tmpTallyName.equals(tallyNewName)
					&& !tmpTallyName.equals(tallyOldName))
				return false;
		}

		String updateSQL = "update tally_table set tally_name=?,remark=? where tally_name=?";
		db.execSQL(updateSQL, new String[] { tallyNewName, tallyNewRemark,
				tallyOldName });
		return true;
	}

	public static List<String> getAllTallyName(Context context) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String querySQL = "select tally_name from tally_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new
													// String[]{}
		List<String> tallyNames = new ArrayList<String>();
		while (cursor.moveToNext()) {
			tallyNames.add(cursor.getString(0));
		}
		return tallyNames;
	}

	public static boolean addRecord(Context context, String tallyName,
			String[] records, int tag) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String querySQL = "select id from tally_table where tally_name=?";
		Cursor cursor = db.rawQuery(querySQL, new String[] { tallyName });
		cursor.moveToNext();
		String tally_id = cursor.getString(0);
		String insertSQL = "insert into record_table values(null,?,?,?,?,?,?,?)";
		// 数据库执行插入时会自动将String类型的tally_id转为int型
		db.execSQL(insertSQL, new String[] { tally_id, records[0], records[1],
				records[2], records[3], records[4], Integer.toString(tag) });
		return true;
	}

	public static List<List<String>> getRecordsOfATally(Context context,
			String tallyName) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 根据账本名称获取id
		String queryTallyId = "select id from tally_table where tally_name=?";
		Cursor tally_cursor = db.rawQuery(queryTallyId,
				new String[] { tallyName });
		tally_cursor.moveToNext();
		String tally_id = tally_cursor.getString(0);

		// 以下获取记录
		String querySQL = "select tag,money,category,remark,date_time,id from record_table where tally_id=?";
		Cursor record_cursor = db.rawQuery(querySQL, new String[] { tally_id });
		List<List<String>> records = new ArrayList<List<String>>();
		while (record_cursor.moveToNext()) {
			List<String> record = new ArrayList<String>();
			String tag = record_cursor.getString(0);
			record.add(tag);
			if (tag.equals("0"))
				record.add("收入:");
			else
				record.add("支出:");
			record.add(record_cursor.getString(1));
			record.add(record_cursor.getString(2));
			record.add(record_cursor.getString(3));
			record.add(record_cursor.getString(4));
			record.add(record_cursor.getString(5));
			records.add(record);
		}
		return records;
	}

	public static String getDetailRecord(Context context, String tallyId) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 以下获取记录
		String querySQL = "select tag,money,category,date_time,tally_id,merchant,remark from record_table where id=?";
		Cursor record_cursor = db.rawQuery(querySQL, new String[] { tallyId });
		StringBuilder detailRecord = new StringBuilder();
		while (record_cursor.moveToNext()) {
			if (record_cursor.getString(0).equals("0"))
				detailRecord.append("收入:");
			else
				detailRecord.append("支出:");
			detailRecord.append(record_cursor.getString(1) + "\n\n");
			detailRecord.append("类别:");
			detailRecord.append(record_cursor.getString(2) + "\n");
			detailRecord.append("时间:");
			detailRecord.append(record_cursor.getString(3) + "\n");

			// 所在账本
			detailRecord.append("账本:");
			String recordtallyId = record_cursor.getString(4);
			Cursor tallyCursor = db.rawQuery(
					"select tally_name from tally_table where id=?",
					new String[] { recordtallyId });
			tallyCursor.moveToNext();
			detailRecord.append(tallyCursor.getString(0) + "\n");

			detailRecord.append("商家:");
			detailRecord.append(record_cursor.getString(5) + "\n");

			detailRecord.append("备注:");
			detailRecord.append(record_cursor.getString(6));
		}
		return detailRecord.toString();
	}

	public static double[] getMoney(Context context, String tallyName) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 根据账本名称获取id
		String queryTallyId = "select id from tally_table where tally_name=?";
		Cursor tally_cursor = db.rawQuery(queryTallyId,
				new String[] { tallyName });
		tally_cursor.moveToNext();
		String tally_id = tally_cursor.getString(0);

		double[] money = new double[2];// 第一个元素存储收入，第二个元素存储支出
		// 以下获取记录
		String querySQL = "select tag,money from record_table where tally_id=?";
		Cursor cursor = db.rawQuery(querySQL, new String[] { tally_id });
		while (cursor.moveToNext()) {
			if (cursor.getString(0).equals("0")) {
				money[0] += Double.parseDouble(cursor.getString(1));
			} else {
				money[1] += Double.parseDouble(cursor.getString(1));
			}
		}
		money[0] = Double
				.valueOf(String.format(Locale.CHINA, "%.2f", money[0]));
		money[1] = Double
				.valueOf(String.format(Locale.CHINA, "%.2f", money[1]));
		return money;
	}

	public static boolean delRecord(Context context, String record_id) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String delSQL = "delete from record_table where id=?";
		db.execSQL(delSQL, new String[] { record_id });
		return true;
	}

	public static List<String> getAllCategory(Context context) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String querySQL = "select category_name from category_table";
		Cursor cursor = db.rawQuery(querySQL, null);
		List<String> categorysList = new ArrayList<String>();
		while (cursor.moveToNext()) {
			categorysList.add(cursor.getString(0));
		}
		return categorysList;
	}

	public static boolean deleteCategory(View itemView) {

		String category_name = ((TextView) (itemView
				.findViewById(R.id.category_text_view))).getText().toString();
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(itemView.getContext(), "tallys",
					null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 类别所属的记录的类别更新为“无类别”
		String updateSQL = "update  record_table set category =? where category=?";
		db.execSQL(updateSQL, new String[] { "无类别", category_name });

		// 删除账本
		String deleteSQL = "delete from category_table where category_name = ?";
		db.execSQL(deleteSQL, new String[] { category_name });

		return true;
	}

	public static boolean addCategory(Context context, String category_name) {
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		// 先检查数据库中是否已经有了此账本
		String querySQL = "select category_name from category_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new
													// String[]{}
		while (cursor.moveToNext()) {
			String tmpTallyName = cursor.getString(0);
			if (tmpTallyName.equals(category_name))
				return false;
		}

		// 第一列设置了autoincrement属性，所以插入记录是为之赋null
		String insertSQL = "insert into category_table values(null,?)";
		db.execSQL(insertSQL, new String[] { category_name });
		return true;
	}

	public static boolean editCategory(View itemView, String categoryNewName) {
		Context context = itemView.getContext();
		if (dbHelper == null)
			dbHelper = new TallyDatabaseHelper(context, "tallys.db", null, 1);
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		String categoryOldName = ((TextView) itemView
				.findViewById(R.id.category_text_view)).getText().toString();

		// 先检查数据库中是否已经有了此账本
		String querySQL = "select category_name from category_table";
		Cursor cursor = db.rawQuery(querySQL, null);// 如果无替换参数，那么就传入null或者new
													// String[]{}
		while (cursor.moveToNext()) {
			String tmpCategoryName = cursor.getString(0);
			if (tmpCategoryName.equals(categoryNewName)
					&& !tmpCategoryName.equals(categoryOldName))
				return false;
		}

		String updateSQL = "update category_table set category_name=? where category_name=?";
		db.execSQL(updateSQL, new String[] { categoryNewName, categoryOldName });
		return true;
	}
}
