package net.wws.data_util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class TallyDatabaseHelper extends SQLiteOpenHelper {

	// 创建账本表的SQL
	private final String CREATE_TALLYS_TABLE_SQL = "create table tally_table (id integer primary key autoincrement,tally_name text not null,remark text,date_time text)";

	//sqlite数据库的real类型不会保存多余的数据，例如保存999.00它会舍去00，而999.12则不会舍去12
	// 创建记录表的SQL,最后一列tag为0表示收入，1表示支出
	private final String CREATE_RECORD_TABLE = "create table record_table (id integer primary key autoincrement,tally_id integer,money text not null,category text,date_time text,merchant text,remark text,tag integer,foreign key(id) references tallys_table(id))";

	// 创建类别表的SQL
	private final String CREATE_CATEGORY_TABLE = "create table category_table (id integer primary key autoincrement,category_name text not null)";

	public TallyDatabaseHelper(Context context, String name,
			CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TALLYS_TABLE_SQL);
		db.execSQL(CREATE_RECORD_TABLE);
		db.execSQL(CREATE_CATEGORY_TABLE);

		// 为category_table插入一些预定的数据
		String[] some_category = {"购物", "交通", "餐饮", "工资", "娱乐", "其他" };
		String insertSQL = "insert into category_table values(null,?)";
		for (String data : some_category) {
			db.execSQL(insertSQL, new String[] { data });
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
