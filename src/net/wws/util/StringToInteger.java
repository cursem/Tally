package net.wws.util;

public class StringToInteger {

	private static boolean isSpace(char x) {
		if (x == ' ' || x == '\t' || x == '\n' || x == '\f' || x == '\b'
				|| x == '\r')
			return true;
		else
			return false;
	}

	private static boolean isDigit(int x) {
		if (x <= '9' && x >= '0')
			return true;
		else
			return false;
	}

	public static int atoi(String numStr) {
		char c; // 当前char
		long total;

		char sign; // 正负标志

		int idx = 0;
		//跳过空格
		while (isSpace(numStr.charAt(idx)))
			++idx;
		c = numStr.charAt(idx++);
		sign = c;
		if (c == '-' || c == '+')
			c = numStr.charAt(idx++); //跳过符号

		total = 0;

		while (isDigit(c)) {
			total = 10 * total + (c - '0');
			if (total > Integer.MAX_VALUE)
				return 0;
			c = numStr.charAt(idx++); 
		}

		if (sign == '-')
			return (int) -total;
		else
			return (int) total;
	}
}
