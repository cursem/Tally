package net.wws.tally;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.wws.data_util.DAOUtil;
import net.wws.tally.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class MainActivity extends Activity {
	final int CATEGORY_REQ_CODE = 0;
	final int MAN_TALLY_REQ_CODE = 1;

	private boolean noTalyyTag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		buildSpinner();

		initWidget();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.actionbar_save_income_outcome_btn) {
			onSaveButtonClick();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (DAOUtil.dbHelper != null)
			DAOUtil.dbHelper.close();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == CATEGORY_REQ_CODE) {
			TextView categoryTextView = (TextView) findViewById(R.id.category_text_view);
			String current_category = categoryTextView.getText().toString();
			List<String> categorys = DAOUtil.getAllCategory(this);
			for (String category : categorys) {
				if (category.equals(current_category))
					return;
			}
			// 之前选择的类别在管理类别后删除了，那么设置为“无类别”
			categoryTextView.setText(R.string.no_category_str);
		} else if (requestCode == MAN_TALLY_REQ_CODE) {
			buildSpinner();
		}
	}

	// 以下是相应的监听方法

	// 类别选择
	public void onCategoryTextViewClick(View source) {
		LinearLayout ll = (LinearLayout) getLayoutInflater().inflate(
				R.layout.activity_main_category_dialog, null);
		ListView listView = (ListView) ll.findViewById(R.id.category_list_view);
		List<String> categorys = DAOUtil.getAllCategory(this);
		categorys.add(0, "无类别");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				R.layout.activity_main_category_dialog_item, categorys);
		listView.setAdapter(adapter);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_select_category_title);
		builder.setView(listView);
		builder.setPositiveButton(R.string.dialog_select_category_btn,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(MainActivity.this,
								ManCategoryActivity.class);
						startActivityForResult(intent, CATEGORY_REQ_CODE);
					}

				});
		builder.setView(ll);
		builder.create();
		AlertDialog alertDialog = builder.show();

		listView.setOnItemClickListener(new CategorySelectedListener(
				alertDialog));
	}

	// 日期选择
	public void onDateTextViewClick(View source) {
		final LinearLayout datePickerLayout = (LinearLayout) getLayoutInflater()
				.inflate(R.layout.activity_main_date_dialog, null);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_select_date_title);
		builder.setView(datePickerLayout);
		builder.setPositiveButton(R.string.complete_select_btn,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						DatePicker datePicker = (DatePicker) datePickerLayout
								.findViewById(R.id.date_picker);
						int y = datePicker.getYear();
						int m = datePicker.getMonth() + 1;// getMonth是从0开始的
						String m_str = m < 10 ? ("0" + m) : ("" + m);// 个位前添加0
						int d = datePicker.getDayOfMonth();
						String d_str = d < 10 ? ("0" + d) : ("" + d);
						String date = y + "-" + m_str + "-" + d_str;
						TextView dateTextView = (TextView) MainActivity.this
								.findViewById(R.id.date_text_view);
						dateTextView.setText(date);
					}
				});
		builder.create().show();
	}

	// 时间选择
	public void onTimeTextViewClick(View source) {
		final LinearLayout timePickerLayout = (LinearLayout) getLayoutInflater()
				.inflate(R.layout.activity_main_time_dialog, null);
		final TimePicker timePicker = (TimePicker) timePickerLayout
				.findViewById(R.id.time_picker);
		timePicker.setIs24HourView(true);
		SimpleDateFormat timef = new SimpleDateFormat("HH:mm", Locale.CHINA);
		String[] hourMinute = timef.format(new Date()).split(":");
		timePicker.setCurrentHour(Integer.valueOf(hourMinute[0]));
		timePicker.setCurrentMinute(Integer.valueOf(hourMinute[1]));

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_select_time_title);
		builder.setView(timePickerLayout);
		builder.setPositiveButton(R.string.complete_select_btn,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						int hour = timePicker.getCurrentHour();
						String hour_str = hour < 10 ? ("0" + hour)
								: ("" + hour);
						int minute = timePicker.getCurrentMinute();
						String minute_str = minute < 10 ? ("0" + minute)
								: ("" + minute);
						TextView timeTextView = (TextView) MainActivity.this
								.findViewById(R.id.time_text_view);
						timeTextView.setText(hour_str + ":" + minute_str);
					}
				});
		builder.create().show();
	}

	public void startManTallyActivity(View view) {
		Intent intent = new Intent(MainActivity.this, ManTallyActivity.class);
		startActivityForResult(intent, MAN_TALLY_REQ_CODE);
	}

	public void startRecordActivity(View view) {
		Intent intent = new Intent(MainActivity.this, RecordActivity.class);
		startActivity(intent);
	}

	public void startPieActivity(View view) {
		Intent intent = new Intent(MainActivity.this, PieActivity.class);
		startActivity(intent);
	}

	// 保存按钮的监听方法
	private void onSaveButtonClick() {

		if (noTalyyTag) {// 没有账本
			Toast.makeText(this, R.string.in_out_activity_no_tally_toast,
					Toast.LENGTH_SHORT).show();
			return;
		}
		TextView moneyTV = (TextView) findViewById(R.id.money_text_view);
		// EditText.getText()会只能地将.25、99.转为正确的25、99
		String money = moneyTV.getText().toString();
		if (money.equals("") || money.equals(".")) {
			Toast.makeText(this,
					R.string.in_out_activity_not_input_money_toast,
					Toast.LENGTH_SHORT).show();
			return;
		}
		money = String.format(Locale.CHINA, "%.2f",
				new Object[] { Double.valueOf(money) });

		TextView categoryTV = (TextView) findViewById(R.id.category_text_view);
		TextView dateTV = (TextView) findViewById(R.id.date_text_view);
		TextView timeTV = (TextView) findViewById(R.id.time_text_view);
		TextView merchantTV = (TextView) findViewById(R.id.merchant_edit_text);
		TextView remarkTV = (TextView) findViewById(R.id.remark_edit_text);

		String category = categoryTV.getText().toString();
		String dateTime = dateTV.getText().toString() + " "
				+ timeTV.getText().toString();
		String merchant = merchantTV.getText().toString();
		String remark = remarkTV.getText().toString();

		if (merchant.equals("")) {
			merchant = "无商家";
		}
		if (remark.equals("")) {
			remark = "无备注";
		}

		String[] records = { money, category, dateTime, merchant, remark };
		Spinner spinner = (Spinner) findViewById(R.id.income_spinner);
		String selected_tally = spinner.getSelectedItem().toString();

		// 是收入还是支出
		RadioGroup rg = (RadioGroup) findViewById(R.id.income_outcome_raido_group);
		int rbId = rg.getCheckedRadioButtonId();
		if (rbId == R.id.income_radio_button)
			rbId = 0;
		else
			rbId = 1;

		DAOUtil.addRecord(this, selected_tally, records, rbId);// 0表示收入

		Toast.makeText(this, R.string.toast_save_success, Toast.LENGTH_SHORT)
				.show();

		merchantTV.setText("");
		remarkTV.setText("");
	}

	private void buildSpinner() {
		// 根据数据库中存在的账本构建Spinner
		List<String> tallyNames = DAOUtil.getAllTallyName(this);
		if (tallyNames.size() == 0) {
			noTalyyTag = true;// 设置没有账本标志
			tallyNames.add("请选择账本");// 为了不显示空白的Spinner，添加一个项目
		} else {
			noTalyyTag = false;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, tallyNames);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);// 设置下拉部分的风格
		Spinner spinner = (Spinner) findViewById(R.id.income_spinner);
		spinner.setAdapter(adapter);
	}

	private void initWidget() {
		TextView categoryTextView = (TextView) findViewById(R.id.category_text_view);
		TextView dateTextView = (TextView) findViewById(R.id.date_text_view);
		TextView timeTextView = (TextView) findViewById(R.id.time_text_view);

		categoryTextView.setText(R.string.no_category_str);

		SimpleDateFormat datef = new SimpleDateFormat("yyy-MM-dd", Locale.CHINA);
		dateTextView.setText(datef.format(new Date()));

		SimpleDateFormat timef = new SimpleDateFormat("HH:mm", Locale.CHINA);
		timeTextView.setText(timef.format(new Date()));
	}

	private class CategorySelectedListener implements
			AdapterView.OnItemClickListener {
		AlertDialog alertDialog;

		public CategorySelectedListener(AlertDialog alertDialog) {
			this.alertDialog = alertDialog;
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			String category = ((TextView) view).getText().toString();
			TextView categoryTextView = (TextView) MainActivity.this
					.findViewById(R.id.category_text_view);
			categoryTextView.setText(category);
			alertDialog.dismiss();
		}
	}
}
