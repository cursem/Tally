package net.wws.tally;

import java.util.List;

import net.wws.data_util.DAOUtil;
import net.wws.id.ActivityResultCode;
import net.wws.tally.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ManTallyActivity extends Activity {
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_man_tally);
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		setResult(ActivityResultCode.MAN_TALLY_RESULT_CODE);
		
		// 显示列表
		reShowTallyItem();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.man_tally, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		} else if (id == R.id.add_tally) {
			createAddTallyDialog();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onDestroy() {
		// 在销毁Activity之前设置结果码以通知之前的Activity执行特定操作
		setResult(2);
		super.onDestroy();
	}

	private void createAddTallyDialog() {
		final TableLayout addTallyLayout = (TableLayout) getLayoutInflater()
				.inflate(R.layout.activity_man_add_edit_tally_dialog, null);

		AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
		alerDialogBuilder.setTitle(R.string.dialog_add_tally_title);
		alerDialogBuilder.setView(addTallyLayout);

		// 设置"确定"按钮
		alerDialogBuilder.setPositiveButton(R.string.dialog_sure_btn,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 获取对话框中的控件(注意调用的是addTallyLayout的findViewById方法)
						EditText tally_name = (EditText) addTallyLayout
								.findViewById(R.id.dialog_tally_name);
						EditText tally_name_remark = (EditText) addTallyLayout
								.findViewById(R.id.dialog_tally_remark);
						String tally_name_str = tally_name.getText().toString();// 若用户没有输入，那么得到的是""
						String tally_name_remark_str = tally_name_remark
								.getText().toString();

						if (tally_name_str.equals("")) {
							Toast.makeText(ManTallyActivity.this,
									R.string.dialog_input_tally_name_hint_toast,
									Toast.LENGTH_SHORT).show();
							createAddTallyDialog();
							return;
						}
						if (tally_name_remark_str.equals("")) {
							tally_name_remark_str = "无备注";
						}
						boolean isadded = DAOUtil.addTally(
								ManTallyActivity.this, tally_name_str,
								tally_name_remark_str);
						if (isadded) {// 添加账本成功，重新显示界面
							reShowTallyItem();
						} else {
							Toast.makeText(ManTallyActivity.this,
									R.string.toast_tally_name_repeat,
									Toast.LENGTH_SHORT).show();
							createAddTallyDialog();
						}
					}
				});

		alerDialogBuilder.setNegativeButton(R.string.dialog_cancle_btn, null);

		alerDialogBuilder.create().show();
	}

	private void reShowTallyItem() {
		LinearLayout storeTallyItem = (LinearLayout) findViewById(R.id.store_tally_item);
		storeTallyItem.removeAllViews();
		List<List<String>> tallys = DAOUtil.getAllTally(this);
		if (tallys.size() == 0) {
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText(R.string.man_activity_no_tally_tip);
			storeTallyItem.addView(tv);
			return;
		}

		for (List<String> tally : tallys) {
			LinearLayout tallyItem = (LinearLayout) getLayoutInflater()
					.inflate(R.layout.activity_man_tally_item, null);
			TextView tallyName = (TextView) tallyItem
					.findViewById(R.id.tally_name);
			TextView tallyRemark = (TextView) tallyItem
					.findViewById(R.id.tally_remark);
			tallyName.setText(tally.get(0));
			tallyRemark.setText(tally.get(1));
			storeTallyItem.addView(tallyItem);
		}
	}

	public void createDeleteDialog(View evtSource) {
		final RelativeLayout itemView = (RelativeLayout) evtSource.getParent();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_delete_tally_title);
		builder.setMessage(R.string.dialog_delete_tally_message);

		builder.setPositiveButton(R.string.dialog_sure_btn, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				boolean isDeleted = DAOUtil.deleteTally(itemView);
				if (isDeleted) {
					reShowTallyItem();
				}
			}
		});
		builder.setNegativeButton(R.string.dialog_cancle_btn, null);
		builder.create();
		builder.show();
	}
	public void createEditDialog(final View evtSource) {
		final RelativeLayout itemView = (RelativeLayout) evtSource.getParent();
		final TableLayout editTallyLayout = (TableLayout) getLayoutInflater()
				.inflate(R.layout.activity_man_add_edit_tally_dialog, null);

		AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
		alerDialogBuilder.setTitle(R.string.dialog_edit_tally_title);
		alerDialogBuilder.setView(editTallyLayout);
		final EditText tallyNameEditText = (EditText) editTallyLayout
				.findViewById(R.id.dialog_tally_name);
		final EditText tallyRemarkEditText = (EditText) editTallyLayout
				.findViewById(R.id.dialog_tally_remark);
		
		final String tallyOldName = ((TextView) itemView.findViewById(R.id.tally_name))
				.getText().toString();
		final String tallyOldRemark = ((TextView) itemView.findViewById(R.id.tally_remark))
				.getText().toString();
		tallyNameEditText.setHint(tallyOldName);
		tallyRemarkEditText.setHint(tallyOldRemark);
		

		// 设置"确定"按钮
		alerDialogBuilder.setPositiveButton(R.string.dialog_sure_btn,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 获取对话框中的控件(注意调用的是addTallyLayout的findViewById方法)
						String tallyNewName = tallyNameEditText.getText().toString();// 若用户没有输入，那么得到的是""
						String tallyNewRemark = tallyRemarkEditText
								.getText().toString();

						if (tallyNewName.equals("")) 
							tallyNewName = tallyOldName;
							
						
						if (tallyNewRemark.equals(""))
							tallyNewRemark = tallyOldRemark;

						boolean edited = DAOUtil.editTally(itemView,
								tallyNewName, tallyNewRemark);
						if (edited) {// 修改账本成功，重新显示界面
							reShowTallyItem();
						} else {
							Toast.makeText(ManTallyActivity.this, R.string.toast_tally_name_repeat,
									Toast.LENGTH_SHORT).show();
							createEditDialog(itemView
									.findViewById(R.id.edit_btn));
						}
					}
				});

		alerDialogBuilder.setNegativeButton(R.string.dialog_cancle_btn, null);

		alerDialogBuilder.create();
		alerDialogBuilder.show();
	}
}
