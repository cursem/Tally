package net.wws.tally;

import java.util.List;

import net.wws.data_util.DAOUtil;
import net.wws.tally.R;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class PieActivity extends Activity {

	Spinner spinner;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pie);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		buildSpinner();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void buildSpinner() {
		spinner = (Spinner) findViewById(R.id.spinner_account_book);
		List<String> tallyNames = DAOUtil.getAllTallyName(this);
		if (tallyNames.size() == 0) {
			LinearLayout pieLayout = (LinearLayout) findViewById(R.id.pie_layout);
			pieLayout.removeAllViews();
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText(R.string.pie_activity_no_tally_tip);
			pieLayout.addView(tv);

			tallyNames.add("请选择账本");// 为了不显示空白的Spinner，添加一个项目
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, tallyNames);
			adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);// 设置下拉部分的风格
			spinner.setAdapter(adapter);
			return;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, tallyNames);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);// 设置下拉部分的风格
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
	}

	private class MyOnItemSelectedListener implements
			AdapterView.OnItemSelectedListener {

		// 以下创建并添加饼状图
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			String tallyName = spinner.getSelectedItem().toString();
			LinearLayout pieLayout = (LinearLayout) findViewById(R.id.pie_layout);
			double values[] = DAOUtil.getMoney(PieActivity.this, tallyName);
			TextView incomeTextView = (TextView) findViewById(R.id.income_textview);
			TextView outcomeTextView = (TextView) findViewById(R.id.outcome_textview);
			incomeTextView.setText(String.valueOf(values[0]));
			outcomeTextView.setText(String.valueOf(values[1]));
			if (values[0] + values[1] == 0) {
				pieLayout.removeAllViews();
				return;
			}

			CategorySeries dataset = buildCategoryDataset("账本统计图", values);
			DefaultRenderer renderer = buildCategoryRenderer(tallyName);
			GraphicalView pieChartView = ChartFactory.getPieChartView(
					PieActivity.this.getBaseContext(), dataset, renderer);// 饼状图
			pieLayout.removeAllViews();
			pieLayout.addView(pieChartView);
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}

		private CategorySeries buildCategoryDataset(String title,
				double[] values) {
			CategorySeries series = new CategorySeries(title);
			series.add("收入", values[0]);
			series.add("支出", values[1]);
			return series;
		}

		private DefaultRenderer buildCategoryRenderer(String title) {
			DefaultRenderer renderer = new DefaultRenderer();

			renderer.setZoomEnabled(true);// 允许放大缩小
			renderer.setZoomButtonsVisible(true);// 显示放大缩小按钮
			renderer.setShowLabels(false);// 不显示带指示线的标记的文字
			// renderer.setShowLegend(false);// 不显示左下角标注
			renderer.setLegendTextSize(36);// 左下角标注文字大小
			renderer.setDisplayValues(true);// 显示扇区数值

			// 设置带指示线的标记的文字和扇区数值的文字大小和颜色
			renderer.setLabelsTextSize(36);
			renderer.setLabelsColor(Color.BLACK);// 饼图上标记文字的颜色

			renderer.setLegendHeight(0);

			// 收入扇区
			SimpleSeriesRenderer incomeSeries = new SimpleSeriesRenderer();
			incomeSeries.setColor(Color.parseColor("#33CC33"));
			renderer.addSeriesRenderer(incomeSeries);

			// 支出扇区
			SimpleSeriesRenderer outcomeSeries = new SimpleSeriesRenderer();
			outcomeSeries.setColor(Color.parseColor("#6666CC"));
			renderer.addSeriesRenderer(outcomeSeries);
			return renderer;
		}
	}
}
