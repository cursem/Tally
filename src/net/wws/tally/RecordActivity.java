package net.wws.tally;

import java.util.List;

import net.wws.data_util.DAOUtil;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class RecordActivity extends Activity {

	Spinner spinner;
	LinearLayout storeItemLayout;// 盛装所有记录的LinearLayout

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_record);
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		buildSpinner();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		 getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void buildSpinner() {
		List<String> tallyNames = DAOUtil.getAllTallyName(this);
		if (tallyNames.size() == 0) {
			storeItemLayout = (LinearLayout) RecordActivity.this
					.findViewById(R.id.store_item);
			storeItemLayout.removeAllViews();
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText(R.string.record_activity_no_tally_tip);
			storeItemLayout.addView(tv);
			
			tallyNames.add("请选择账本");
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_dropdown_item, tallyNames);
			adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);// 设置下拉部分的风格
			spinner = (Spinner) findViewById(R.id.record_spinner);
			spinner.setAdapter(adapter);
			return;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, tallyNames);
		adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);// 设置下拉部分的风格
		spinner = (Spinner) findViewById(R.id.record_spinner);
		spinner.setAdapter(adapter);
		// 程序每次构建Spinner时也会触发这个监听器
		spinner.setOnItemSelectedListener(new MyOnItemSelectedListener());
	}

	private class MyOnItemSelectedListener implements
			AdapterView.OnItemSelectedListener {

		// 构建Spinner完成也会触发此方法
		@Override
		public void onItemSelected(AdapterView<?> parent, View view,
				int position, long id) {
			reShowRecordItem();
		}

		@Override
		public void onNothingSelected(AdapterView<?> parent) {
		}

	}

	// 点击记录的监听方法
	public void createDetailDialog(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_detail_title);
		LinearLayout rootLayout = (LinearLayout) findViewById(R.layout.activity_record);
		LinearLayout dialogContent = (LinearLayout) getLayoutInflater()
				.inflate(R.layout.activity_record_dialog, rootLayout);
		builder.setView(dialogContent);
		String recordId = ((TextView) view.findViewById(R.id.record_id_text))
				.getText().toString();
		String detailItem = DAOUtil.getDetailRecord(view.getContext(),
				recordId);
		TextView detailTextView = (TextView) dialogContent
				.findViewById(R.id.record_detail_tv);
		detailTextView.setText(detailItem);

		builder.setPositiveButton(R.string.dialog_sure_btn, null);
		builder.setNegativeButton(R.string.dialog_detail_delete_btn, new DelRecordListener(recordId));

		builder.create().show();
	}

	private class DelRecordListener implements DialogInterface.OnClickListener {

		String recordId;

		public DelRecordListener(String recordId) {
			this.recordId = recordId;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					RecordActivity.this);
			builder.setTitle(R.string.dialog_delete_record_title);
			builder.setMessage(R.string.dialog_delete_record_message);
			builder.setPositiveButton(R.string.dialog_sure_btn,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							boolean deleted = DAOUtil.delRecord(
									RecordActivity.this, recordId);
							if (deleted) {
								reShowRecordItem();
							}
						}
					});
			builder.setNegativeButton(R.string.dialog_cancle_btn, null);
			builder.create().show();
		}
	}

	// 重新显示账本的记录
	private void reShowRecordItem() {
		// 删除原有的item
		if (storeItemLayout == null)
			storeItemLayout = (LinearLayout) RecordActivity.this
					.findViewById(R.id.store_item);
		storeItemLayout.removeAllViews();

		// 获取选定的账本名
		String tallyName = spinner.getSelectedItem().toString();
		List<List<String>> records = DAOUtil.getRecordsOfATally(
				RecordActivity.this, tallyName);

		if (records.size() == 0) {
			TextView tv = new TextView(RecordActivity.this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText(R.string.tally_no_record_tip);
			storeItemLayout.addView(tv);
			return;
		}

		for (List<String> record : records) {
			LinearLayout recordItem = (LinearLayout) RecordActivity.this
					.getLayoutInflater().inflate(R.layout.activity_record_item,
							null);// 传入null，表示这个Layout没有父容器
			TextView record_income_outcome = (TextView) recordItem
					.findViewById(R.id.record_income_outcome);
			TextView income_outcome_money = (TextView) recordItem
					.findViewById(R.id.income_outcome_money);
			TextView record_category = (TextView) recordItem
					.findViewById(R.id.record_category);
			TextView record_remark = (TextView) recordItem
					.findViewById(R.id.record_remark);
			TextView record_date = (TextView) recordItem
					.findViewById(R.id.record_date);
			TextView record_id = (TextView) recordItem
					.findViewById(R.id.record_id_text);

			record_income_outcome.setText(record.get(1));
			income_outcome_money.setText(record.get(2));
			record_category.setText(record.get(3));
			record_remark.setText(record.get(4));
			record_date.setText(record.get(5));
			record_id.setText(record.get(6));

			if (record.get(0).equals("0")) {
				income_outcome_money.setTextColor(Color.parseColor("#33CC33"));
			} else {
				income_outcome_money.setTextColor(Color.parseColor("#6666CC"));
			}

			// 添加记录项
			storeItemLayout.addView(recordItem);
		}
	}
}
