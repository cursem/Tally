package net.wws.tally;

import java.util.List;

import net.wws.data_util.DAOUtil;
import net.wws.id.ActivityResultCode;
import net.wws.tally.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ManCategoryActivity extends Activity {
	private ActionBar actionBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_man_category);
		actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);

		setResult(ActivityResultCode.MAN_CATEGORY_RESULT_CODE);
		
		// 显示列表
		reShowCategoryItem();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.man_category, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == android.R.id.home) {
			finish();
			return true;
		} else if (id == R.id.add_category) {
			createAddCategoryDialog();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDestroy() {
		// 在销毁Activity之前设置结果码已通知之前的Activity执行特定操作
		super.onDestroy();
	}

	private void createAddCategoryDialog() {
		final TableLayout addCategoryLayout = (TableLayout) getLayoutInflater()
				.inflate(R.layout.activity_man_add_edit_category_dialog, null);

		AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
		alerDialogBuilder.setTitle(R.string.dialog_add_category_title);
		alerDialogBuilder.setView(addCategoryLayout);

		// 设置"确定"按钮
		alerDialogBuilder.setPositiveButton(R.string.dialog_sure_btn,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 获取对话框中的控件(注意调用的是addTallyLayout的findViewById方法)
						EditText categoryEditText = (EditText) addCategoryLayout
								.findViewById(R.id.dialog_category_name);
						String category_name = categoryEditText.getText()
								.toString();// 若用户没有输入，那么得到的是""

						if (category_name.equals("")) {
							Toast.makeText(ManCategoryActivity.this,
									R.string.dialog_category_name_hint_toast,
									Toast.LENGTH_SHORT).show();
							createAddCategoryDialog();
							return;
						}
						boolean isadded = DAOUtil.addCategory(
								ManCategoryActivity.this, category_name);
						if (isadded) {// 添加类别成功，重新显示界面
							reShowCategoryItem();
						} else {
							Toast.makeText(ManCategoryActivity.this,
									R.string.toast_category_name_repeat,
									Toast.LENGTH_SHORT).show();
							createAddCategoryDialog();
						}
					}
				});

		alerDialogBuilder.setNegativeButton(R.string.dialog_cancle_btn, null);

		alerDialogBuilder.create().show();
	}

	private void reShowCategoryItem() {
		LinearLayout storeCategoryItem = (LinearLayout) findViewById(R.id.store_category_item);
		storeCategoryItem.removeAllViews();
		List<String> categorys = DAOUtil.getAllCategory(this);
		if (categorys.size() == 0) {
			TextView tv = new TextView(this);
			tv.setLayoutParams(new ViewGroup.LayoutParams(
					ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.WRAP_CONTENT));
			tv.setGravity(Gravity.CENTER);
			tv.setText(R.string.no_category_tip);
			storeCategoryItem.addView(tv);
			return;
		}

		for (String category : categorys) {
			LinearLayout categoryItem = (LinearLayout) getLayoutInflater()
					.inflate(R.layout.activity_man_category_item, null);
			TextView categoryTextView = (TextView) categoryItem
					.findViewById(R.id.category_text_view);
			categoryTextView.setText(category);
			storeCategoryItem.addView(categoryItem);
		}
	}

	public void createDeleteDialog(View evtSource) {
		final RelativeLayout itemView = (RelativeLayout) evtSource.getParent();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.dialog_delete_category_title);
		builder.setMessage(R.string.dialog_delete_category_message);

		builder.setPositiveButton(R.string.dialog_sure_btn,
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						boolean isDeleted = DAOUtil.deleteCategory(itemView);
						if (isDeleted) {
							reShowCategoryItem();
						}
					}
				});
		builder.setNegativeButton(R.string.dialog_cancle_btn, null);
		builder.create();
		builder.show();
	}

	public void createEditDialog(View evtSource) {
		final RelativeLayout itemView = (RelativeLayout) evtSource.getParent();
		final TableLayout editTallyLayout = (TableLayout) getLayoutInflater()
				.inflate(R.layout.activity_man_add_edit_category_dialog, null);

		AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(this);
		alerDialogBuilder.setTitle(R.string.dialog_edit_category_title);
		alerDialogBuilder.setView(editTallyLayout);

		final EditText categoryEditText = (EditText) editTallyLayout
				.findViewById(R.id.dialog_category_name);
		final String categoryOldName = ((TextView) itemView
				.findViewById(R.id.category_text_view)).getText().toString();
		categoryEditText.setHint(categoryOldName);

		// 设置"确定"按钮
		alerDialogBuilder.setPositiveButton(R.string.dialog_sure_btn,
				new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// 获取对话框中的控件(注意调用的是addTallyLayout的findViewById方法)
						String categoryNewName = categoryEditText.getText()
								.toString();// 若用户没有输入，那么得到的是""

						if (categoryNewName.equals("")) {
							categoryNewName = categoryOldName;
							return;
						}
						boolean edited = DAOUtil.editCategory(itemView,
								categoryNewName);
						if (edited) {// 修改账本成功，重新显示界面
							reShowCategoryItem();
						} else {
							Toast.makeText(ManCategoryActivity.this,
									R.string.toast_category_name_repeat,
									Toast.LENGTH_SHORT).show();
							createEditDialog(itemView
									.findViewById(R.id.edit_btn));
						}
					}
				});

		alerDialogBuilder.setNegativeButton(R.string.dialog_cancle_btn, null);

		alerDialogBuilder.create();
		alerDialogBuilder.show();
	}
}
